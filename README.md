# Pokemon Map #

The live web app can be accessed at [https://pokemon-map-userreport.herokuapp.com/](https://pokemon-map-userreport.herokuapp.com/)

### About this project ###
This is a learning project inspired by the popular mobile game "Pokemon Go". The goal was to learn how to utilize Esri's ArcGIS Javascript API v1.7 to develop a web app. 

### Screen Shots ###
![Map View](screenshots/Pokemon-Map-View.jpg)
Shows all submitted pokemon sightings.

![Search View](screenshots/Pokemon-Map-Search.jpg)
Allows user to search by name or type.

![Submit Report UI](screenshots/Pokemon-Map-Report.jpg)
Interface for users to submit pokemon sightings.