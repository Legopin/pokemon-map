require([
  'esri/map',
  'esri/layers/FeatureLayer',
  'esri/dijit/PopupTemplate',
  'esri/dijit/Search',
  'esri/geometry/webMercatorUtils',
  'esri/geometry/Point',
  'esri/graphic',
  'esri/request',
  'dojo/parser',
  'dojo/store/Memory',
  'dijit/form/FilteringSelect',
  'dojo/on',
  'dojo/_base/array',
  'dijit/form/Button',
  'dojo/request/xhr',
  'dojo/dom-form',
  'esri/toolbars/draw',
  'esri/symbols/PictureMarkerSymbol',
  'dijit/registry',
  'dojo/date/locale',
  'esri/dijit/LocateButton',
  'dijit/form/DateTextBox',
  'dijit/form/Select',
  'dijit/form/ValidationTextBox',
  'dijit/form/Form',
  'dojo/domReady!'
], function (
  Map,
  FeatureLayer,
  PopupTemplate,
  Search,
  webMercatorUtils,
  Point,
  Graphic,
  esriRequest,
  parser,
  Memory,
  FilteringSelect,
  on,
  array,
  Button,
  xhr,
  domForm,
  Draw,
  PictureMarkerSymbol,
  registry,
  locale,
  LocateButton
) {
  parser.parse()
  var map = new Map('map', {
    basemap: 'gray',
    center: [-118.016, 33.903], // lon, lat
    zoom: 12
  })

  var picMarker = new PictureMarkerSymbol(
    '/images/map-marker-icon-select.png',
    20,
    20)

  // paste in demo code for feature collection
  // hide the popup if its outside the map's extent
  var featureLayer
  
   map.on('mouse-drag', function (evt) {
     if (map.infoWindow.isShowing) {
       var loc = map.infoWindow.getSelectedFeature().geometry
       if (!map.extent.contains(loc)) {
         map.infoWindow.hide()
       }
     }
   })

  // create a feature collection for the catch reports
  var featureCollection = {
    'layerDefinition': null,
    'featureSet': {
      'features': [],
      'geometryType': 'esriGeometryPoint'
    }
  }
  featureCollection.layerDefinition = {
    'geometryType': 'esriGeometryPoint',
    'objectIdField': 'ObjectID',
    'drawingInfo': {
      'renderer': {
        'type': 'simple',
        'symbol': {
          'type': 'esriPMS',
          'url': '/images/map-marker-icon.png',
          'contentType': 'image/png',
          'width': 15,
          'height': 15
        }
      }
    },
    'fields': [{
      'name': 'ObjectID',
      'alias': 'ObjectID',
      'type': 'esriFieldTypeOID'
    }, {
      'name': 'pokemon_name',
      'alias': 'Pokemon_name',
      'type': 'esriFieldTypeString'
    }, {
      'name': 'capture_time_of_day',
      'alias': 'Time of day',
      'type': 'esriFieldTypeString'
    }, {
      'name': 'capture_date',
      'alias': 'Date',
      'type': 'esriFieldTypeString'
    }]
  }

  // define a popup template
  var popupTemplate = new PopupTemplate({
    title: '{pokemon_name}',
    fieldInfos: [
      {
        fieldName: 'pokemon_type',
        label: 'Type:',
        visible: true
      },
      {
        fieldName: 'capture_date',
        label: 'Date:',
        visible: true
      },
      {
        fieldName: 'capture_time_of_day',
        label: 'Time of Day:',
        visible: true
      }
    ]
  })

  // create a feature layer based on the feature collection
  featureLayer = new FeatureLayer(featureCollection, {
    id: 'catchLayer',
    infoTemplate: popupTemplate
  })

  // add the feature layer that contains the catch reports to the map
  map.addLayers([featureLayer])

  var tb = new Draw(map)

  map.on('load', initControls)

  function initControls () {
    var search = new Search({
      map: map,
      graphicsLayer: featureLayer
    }, 'search')
    search.startup()
    initDijits()
    tb.on('draw-end', addGraphic)

    geoLocate = new LocateButton({
      map: map,
      highlightLocation: false
    }, 'LocateButton')
    geoLocate.startup()
  }

  function addGraphic (evt) {
    tb.deactivate()
    map.enableMapNavigation()
    map.graphics.add(new Graphic(evt.geometry, picMarker))
    var mp = webMercatorUtils.webMercatorToGeographic(evt.geometry)
    var longitude = mp.x
    var latitude = mp.y
    var location_textbox = registry.byId('capture_location')
    location_textbox.set('value', latitude + ', ' + longitude)
  }

  // associate the features with the popup on click
  featureLayer.on('click', function (evt) {
    map.infoWindow.setFeatures([evt.graphic])
  })

  map.on('layers-add-result', function (results) {
    requestCatchReports('all')
  })

  // supply argument all if needing all catch_reports, supply pokemon name for specific pokemons
  function requestCatchReports (pokemonArg, pokemonQuery) {
    var urlString = '/api/catch_reports'
    if (pokemonArg !== 'all' && pokemonArg !== '') {
      urlString = urlString + '/' + pokemonArg
    }
    else if (typeof pokemonQuery !== 'undefined' && pokemonQuery !== '') {
      urlString = urlString + '?type=' + pokemonQuery
    }

    var requestHandle = esriRequest({
      url: urlString,
      handleAs: 'json'
    })
    requestHandle.then(requestSucceeded, requestFailed)
  }

  function requestSucceeded (response, io) {
    // loop through the items and add to the feature layer
    var features = []
    array.forEach(response, function (item) {
      var attr = {}
      attr['pokemon_name'] = item.pokemon.name
      attr['pokemon_type'] = item.pokemon.type
      attr['capture_time_of_day'] = item.capture_time_of_day
      var dateString = locale.format(new Date(item.capture_date), {
        selector: 'date',
        formatLength: 'short'
      })
      attr['capture_date'] = dateString

      var geometry = new Point(item)

      var graphic = new Graphic(geometry)
      graphic.setAttributes(attr)
      features.push(graphic)
    })

    featureLayer.applyEdits(features, null, null)
  }

  function requestFailed (error) {
    console.log('request failed:')
    console.log(error)
  }

  // submit catch reports
  function submitCatchReports () {
    var formObj = domForm.toObject('catch_form')
    xhr.post('/api/catch_reports/', {
      data: formObj
    })
  }

  function initDijits () {
    // setup filtering select
    var pokemon_store = new Memory({
      data: [
        {name: 'Bulbasaur', id: 'Bulbasaur'},
        {name: 'Ivysaur', id: 'Ivysaur'},
        {name: 'Venusaur', id: 'Venusaur'},
        {name: 'Charmander', id: 'Charmander'},
        {name: 'Charmeleon', id: 'Charmeleon'},
        {name: 'Charizard', id: 'Charizard'},
        {name: 'Squirtle', id: 'Squirtle'},
        {name: 'Wartortle', id: 'Wartortle'},
        {name: 'Blastoise', id: 'Blastoise'},
        {name: 'Caterpie', id: 'Caterpie'},
        {name: 'Metapod', id: 'Metapod'},
        {name: 'Butterfree', id: 'Butterfree'},
        {name: 'Weedle', id: 'Weedle'},
        {name: 'Kakuna', id: 'Kakuna'},
        {name: 'Beedrill', id: 'Beedrill'},
        {name: 'Pidgey', id: 'Pidgey'},
        {name: 'Pidgeotto', id: 'Pidgeotto'},
        {name: 'Pidgeot', id: 'Pidgeot'},
        {name: 'Rattata', id: 'Rattata'},
        {name: 'Raticate', id: 'Raticate'},
        {name: 'Spearow', id: 'Spearow'},
        {name: 'Fearow', id: 'Fearow'},
        {name: 'Ekans', id: 'Ekans'},
        {name: 'Arbok', id: 'Arbok'},
        {name: 'Pikachu', id: 'Pikachu'},
        {name: 'Raichu', id: 'Raichu'},
        {name: 'Sandshrew', id: 'Sandshrew'},
        {name: 'Sandslash', id: 'Sandslash'},
        {name: 'Nidoran-F', id: 'Nidoran-F'},
        {name: 'Nidorina', id: 'Nidorina'},
        {name: 'Nidoqueen', id: 'Nidoqueen'},
        {name: 'Nidoran-M', id: 'Nidoran-M'},
        {name: 'Nidorino', id: 'Nidorino'},
        {name: 'Nidoking', id: 'Nidoking'},
        {name: 'Clefairy', id: 'Clefairy'},
        {name: 'Clefable', id: 'Clefable'},
        {name: 'Vulpix', id: 'Vulpix'},
        {name: 'Ninetales', id: 'Ninetales'},
        {name: 'Jigglypuff', id: 'Jigglypuff'},
        {name: 'Wigglytuff', id: 'Wigglytuff'},
        {name: 'Zubat', id: 'Zubat'},
        {name: 'Golbat', id: 'Golbat'},
        {name: 'Oddish', id: 'Oddish'},
        {name: 'Gloom', id: 'Gloom'},
        {name: 'Vileplume', id: 'Vileplume'},
        {name: 'Paras', id: 'Paras'},
        {name: 'Parasect', id: 'Parasect'},
        {name: 'Venonat', id: 'Venonat'},
        {name: 'Venomoth', id: 'Venomoth'},
        {name: 'Diglett', id: 'Diglett'},
        {name: 'Dugtrio', id: 'Dugtrio'},
        {name: 'Meowth', id: 'Meowth'},
        {name: 'Persian', id: 'Persian'},
        {name: 'Psyduck', id: 'Psyduck'},
        {name: 'Golduck', id: 'Golduck'},
        {name: 'Mankey', id: 'Mankey'},
        {name: 'Primeape', id: 'Primeape'},
        {name: 'Growlithe', id: 'Growlithe'},
        {name: 'Arcanine', id: 'Arcanine'},
        {name: 'Poliwag', id: 'Poliwag'},
        {name: 'Poliwhirl', id: 'Poliwhirl'},
        {name: 'Poliwrath', id: 'Poliwrath'},
        {name: 'Abra', id: 'Abra'},
        {name: 'Kadabra', id: 'Kadabra'},
        {name: 'Alakazam', id: 'Alakazam'},
        {name: 'Machop', id: 'Machop'},
        {name: 'Machoke', id: 'Machoke'},
        {name: 'Machamp', id: 'Machamp'},
        {name: 'Bellsprout', id: 'Bellsprout'},
        {name: 'Weepinbell', id: 'Weepinbell'},
        {name: 'Victreebel', id: 'Victreebel'},
        {name: 'Tentacool', id: 'Tentacool'},
        {name: 'Tentacruel', id: 'Tentacruel'},
        {name: 'Geodude', id: 'Geodude'},
        {name: 'Graveler', id: 'Graveler'},
        {name: 'Golem', id: 'Golem'},
        {name: 'Ponyta', id: 'Ponyta'},
        {name: 'Rapidash', id: 'Rapidash'},
        {name: 'Slowpoke', id: 'Slowpoke'},
        {name: 'Slowbro', id: 'Slowbro'},
        {name: 'Magnemite', id: 'Magnemite'},
        {name: 'Magneton', id: 'Magneton'},
        {name: 'Farfetchd', id: 'Farfetchd'},
        {name: 'Doduo', id: 'Doduo'},
        {name: 'Dodrio', id: 'Dodrio'},
        {name: 'Seel', id: 'Seel'},
        {name: 'Dewgong', id: 'Dewgong'},
        {name: 'Grimer', id: 'Grimer'},
        {name: 'Muk', id: 'Muk'},
        {name: 'Shellder', id: 'Shellder'},
        {name: 'Cloyster', id: 'Cloyster'},
        {name: 'Gastly', id: 'Gastly'},
        {name: 'Haunter', id: 'Haunter'},
        {name: 'Gengar', id: 'Gengar'},
        {name: 'Onix', id: 'Onix'},
        {name: 'Drowzee', id: 'Drowzee'},
        {name: 'Hypno', id: 'Hypno'},
        {name: 'Krabby', id: 'Krabby'},
        {name: 'Kingler', id: 'Kingler'},
        {name: 'Voltorb', id: 'Voltorb'},
        {name: 'Electrode', id: 'Electrode'},
        {name: 'Exeggcute', id: 'Exeggcute'},
        {name: 'Exeggutor', id: 'Exeggutor'},
        {name: 'Cubone', id: 'Cubone'},
        {name: 'Marowak', id: 'Marowak'},
        {name: 'Hitmonlee', id: 'Hitmonlee'},
        {name: 'Hitmonchan', id: 'Hitmonchan'},
        {name: 'Lickitung', id: 'Lickitung'},
        {name: 'Koffing', id: 'Koffing'},
        {name: 'Weezing', id: 'Weezing'},
        {name: 'Rhyhorn', id: 'Rhyhorn'},
        {name: 'Rhydon', id: 'Rhydon'},
        {name: 'Chansey', id: 'Chansey'},
        {name: 'Tangela', id: 'Tangela'},
        {name: 'Kangaskhan', id: 'Kangaskhan'},
        {name: 'Horsea', id: 'Horsea'},
        {name: 'Seadra', id: 'Seadra'},
        {name: 'Goldeen', id: 'Goldeen'},
        {name: 'Seaking', id: 'Seaking'},
        {name: 'Staryu', id: 'Staryu'},
        {name: 'Starmie', id: 'Starmie'},
        {name: 'Mr-Mime', id: 'Mr-Mime'},
        {name: 'Scyther', id: 'Scyther'},
        {name: 'Jynx', id: 'Jynx'},
        {name: 'Electabuzz', id: 'Electabuzz'},
        {name: 'Magmar', id: 'Magmar'},
        {name: 'Pinsir', id: 'Pinsir'},
        {name: 'Tauros', id: 'Tauros'},
        {name: 'Magikarp', id: 'Magikarp'},
        {name: 'Gyarados', id: 'Gyarados'},
        {name: 'Lapras', id: 'Lapras'},
        {name: 'Ditto', id: 'Ditto'},
        {name: 'Eevee', id: 'Eevee'},
        {name: 'Vaporeon', id: 'Vaporeon'},
        {name: 'Jolteon', id: 'Jolteon'},
        {name: 'Flareon', id: 'Flareon'},
        {name: 'Porygon', id: 'Porygon'},
        {name: 'Omanyte', id: 'Omanyte'},
        {name: 'Omastar', id: 'Omastar'},
        {name: 'Kabuto', id: 'Kabuto'},
        {name: 'Kabutops', id: 'Kabutops'},
        {name: 'Aerodactyl', id: 'Aerodactyl'},
        {name: 'Snorlax', id: 'Snorlax'},
        {name: 'Articuno', id: 'Articuno'},
        {name: 'Zapdos', id: 'Zapdos'},
        {name: 'Moltres', id: 'Moltres'},
        {name: 'Dratini', id: 'Dratini'},
        {name: 'Dragonair', id: 'Dragonair'},
        {name: 'Dragonite', id: 'Dragonite'},
        {name: 'Mewtwo', id: 'Mewtwo'},
        {name: 'Mew', id: 'Mew'}
      ]
    })

    var types_store = new Memory({
      data: [
        {name: 'Normal', id: 'normal'},
        {name: 'Fighting', id: 'fighting'},
        {name: 'Flying', id: 'flying'},
        {name: 'Poison', id: 'poison'},
        {name: 'Ground', id: 'ground'},
        {name: 'Rock', id: 'rock'},
        {name: 'Bug', id: 'bug'},
        {name: 'Ghost', id: 'ghost'},
        {name: 'Steel', id: 'steel'},
        {name: 'Fire', id: 'fire'},
        {name: 'Water', id: 'water'},
        {name: 'Grass', id: 'grass'},
        {name: 'Electric', id: 'electric'},
        {name: 'Psychic', id: 'psychic'},
        {name: 'Ice', id: 'ice'},
        {name: 'Dragon', id: 'dragon'}
      ]
    })

    var filteringSelect = new FilteringSelect({
      id: 'pokemon_select',
      name: 'pokemon_name',
      store: pokemon_store,
      searchAttr: 'name',
      required: true,
      placeHolder: 'Select Pokemon'
    }, 'pokemon_select').startup()

    var filteringSelect2 = new FilteringSelect({
      id: 'pokemon_list',
      name: 'pokemon_list',
      store: pokemon_store,
      searchAttr: 'name',
      placeHolder: 'Select Pokemon',
      required: false,
      ignoreCase: true,
      onChange: function (value) {
        if (value !== '') {
          var type_filter = registry.byId('pokemon_type')
          type_filter.reset()
        }
      }
    }, 'pokemon_list').startup()

    var pokemon_type = new FilteringSelect({
      id: 'pokemon_type',
      name: 'pokemon_type',
      store: types_store,
      searchAttr: 'name',
      placeHolder: 'Select Type',
      required: false,
      ignoreCase: true,
      onChange: function (value) {
        if (value !== '') {
          var pokemon_filter = registry.byId('pokemon_list')
          pokemon_filter.reset()
        }
      }
    }, 'pokemon_type').startup()

    var searchBtn = new Button({
      label: 'Search',
      onClick: function () {
        var pokemon_filter = registry.byId('pokemon_list').get('value')
        var type_filter = registry.byId('pokemon_type').get('value')
        featureLayer.clear()
        requestCatchReports(pokemon_filter, type_filter)
      }
    }, 'searchBtn').startup()

    var searchClearBtn = new Button({
      label: 'Reset Search Filters',
      onClick: function () {
        search_form.reset()
        featureLayer.clear()
        requestCatchReports('all')
      }
    }, 'searchClearBtn').startup()

    var submitBtn = new Button({
      label: 'Submit',
      onClick: function () {
        submitCatchReports()
        catch_form.reset()
        featureLayer.clear()
        map.graphics.clear()
        requestCatchReports('all')
      }
    }, 'submitBtn').startup()

    var addPointBtn = new Button({
      label: 'Add Point',
      onClick: function () {
        map.graphics.clear()
        tb.activate('point')
        map.disableMapNavigation()
      }
    }, 'addPointBtn').startup()
  }
})
