var express = require('express')
var router = express.Router()

/* GET all Catch Reports as JSON 
OR return queried pokemon types */
router.get('/catch_reports', function (req, res) {
  var db = req.db
  var catch_reports = db.get('catch_reports')
  var find_doc = {}
  if (req.query.type) {
    find_doc = {'pokemon.type': req.query.type}
  }

  catch_reports.find(find_doc, function (err, docs) {
    res.json(docs)
  })
})

// POST form response, create catch report
router.post('/catch_reports', function (req, res) {
  var db = req.db

  var poke_name = req.body.pokemon_name
  var cap_date = req.body.capture_date
  var time_of_day = req.body.capture_time_of_day
  var coordinates = req.body.capture_location
  var latitude = parseFloat(coordinates.split(',')[0])
  var longitude = parseFloat(coordinates.split(',')[1])

  if (poke_name !== '' && coordinates !== '') {
    var poke_collection = db.get('pokemons')
    var collection = db.get('catch_reports')

    var poke_item = poke_collection.find({'name': poke_name}, function (err, docs) {
      collection.insert(
        {'pokemon': docs[0],
          'capture_date': new Date(cap_date),
          'capture_time_of_day': time_of_day,
          'latitude': latitude,
        'longitude': longitude })
    })
  }
  res.redirect('/')
})

// GET all reports of specific pokemon_name
router.get('/catch_reports/:pokemon', function (req, res) {
  var db = req.db
  var collection = db.get('catch_reports')
  collection.find({'pokemon_name': req.params.pokemon}, function (err, docs) {
    res.json(docs)
  })
})

module.exports = router
